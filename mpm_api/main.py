from os import getenv

from dotenv import load_dotenv
from flask import Flask
from flask_admin.contrib.sqla import ModelView
from flask_restx import Api, Resource

from mpm_api.api import blueprint as API_CONFIG
from mpm_api.api.admin import InitializeAdmin
from mpm_api.db import db

# Get the variables from .env
load_dotenv()

# Create the Flask application that is the basis of all Flask-X functions
app = Flask(__name__)
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = f'mysql+mysqldb://{getenv("DB_USER")}:{getenv("DB_PASS")}@{getenv("DB_HOST")}:{getenv("DB_PORT")}/{getenv("DB_NAME")}'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SECRET_KEY"] = getenv("SECRET_KEY")

# Create an API object that will allow us to access the resource portion of our repo in engine
api = Api(app)

# Create admin interface for manipulating db entries:
admin = InitializeAdmin(app)

# Load in the resources
app.register_blueprint(API_CONFIG)

# Boiler-plate run
if __name__ == "__main__":
    db.init_app(app)
    app.run(debug=True)
