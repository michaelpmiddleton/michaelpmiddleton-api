import json

from sqlalchemy.orm import relationship

from mpm_api.db import db


class LocalizedPage(db.Model):
    __tablename__ = "local_page"

    # Establishes foreign keys for use as a secondary table
    page_id = db.Column(db.Integer, db.ForeignKey("meta_page.id"), primary_key=True)
    language_id = db.Column(db.Integer, db.ForeignKey("meta_language.id"), primary_key=True)

    # Establish local hooks to language and page
    page = relationship("Page")
    language = relationship("Language")

    # Store data for localized page
    data = db.Column(db.String(2000), nullable=True)

    def __repr__(self):
        return f"{self.page.name} ({self.language.value})"

    @classmethod
    def find_page_lang_combo(cls, page, lang):
        all_page_versions = cls.query.filter(cls.page_id == page).all()

        if all_page_versions:
            for localized_page in all_page_versions:
                if localized_page.language.equals(lang):
                    # page-language pair found
                    return localized_page

            # language is missing
            raise KeyError

        # page is missing
        else:
            raise FileNotFoundError

    def json(self):
        return json.loads(self.data) if self.data is not None else {}
