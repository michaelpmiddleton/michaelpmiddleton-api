from sqlalchemy.orm import relationship

from mpm_api.db import db


class Project(db.Model):
    __tablename__ = "meta_project"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    gitlab_url = db.Column(db.String(200))
    github_url = db.Column(db.String(200))
    download_url = db.Column(db.String(200))
    img = db.Column(db.String(200))
    project_type = db.Column(db.Integer, default=0, nullable=False)
    languages = relationship("Language", secondary="local_project", lazy="dynamic")

    def __repr__(self):
        return f"{self.id}"
