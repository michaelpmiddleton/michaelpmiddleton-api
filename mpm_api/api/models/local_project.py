from sqlalchemy.orm import relationship

from mpm_api.db import db


class LocalizedProject(db.Model):
    __tablename__ = "local_project"

    # Establishes foreign keys for use as a secondary table
    project_id = db.Column(db.Integer, db.ForeignKey("meta_project.id"), primary_key=True)
    language_id = db.Column(db.Integer, db.ForeignKey("meta_language.id"), primary_key=True)

    # Establish local hooks to language and project
    project = relationship("Project")
    language = relationship("Language")

    # Store data for localized project
    local_name = db.Column(db.String(50))
    description = db.Column(db.String(400))

    def __repr__(self):
        return f"{self.project.name} ({self.language.value})"

    @classmethod
    def find_project_lang_combo(cls, proj, lang):
        all_project_versions = cls.query.filter(cls.project_id == proj).all()

        if all_project_versions:
            for localized_project in all_page_versions:
                if localized_project.language.equals(lang):
                    # page-language pair found
                    return localized_project

            # language is missing
            raise KeyError

        # project is missing
        else:
            raise FileNotFoundError

    @classmethod
    def get_all_by_language(cls, language):
        results = cls.query.filter(cls.language == language).all()

        # Return all of the results if they're not 
        if results:
            return results
            
        raise FileNotFoundError

    def json(self):
        base_json = {
            "id": self.project_id,
            "name": self.local_name,
            "source_control": {
                "gitlab": self.project.gitlab_url,
                "github": self.project.github_url
            },
            "img": self.project.img,
            "description": self.description,                
        }