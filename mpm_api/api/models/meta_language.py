from mpm_api.db import db


class Language(db.Model):
    __tablename__ = "meta_language"

    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.String(2))

    def __repr__(self):
        return f"{self.value} ({self.id})"

    def equals(self, other):
        return self.value == other

    @classmethod
    def find_by_code(cls, code):
        return cls.query.filter(cls.value==code).first()
