from sqlalchemy import Table
from sqlalchemy.orm import relationship

from mpm_api.db import db


class Page(db.Model):
    __tablename__ = "meta_page"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20))
    address = db.Column(db.String(200))
    languages = relationship("Language", secondary="local_page", lazy="dynamic")

    def __repr__(self):
        return f"{self.name} ({self.id})"
