from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView

from mpm_api.api import models
from mpm_api.db import db


class LocalizedPageView(ModelView):
    column_exclude_list = [
        "data",
    ]


class LocalizedProjectView(ModelView):
    column_exclude_list = [
        "description",
    ]


def InitializeAdmin(app):
    admin = Admin(app, "Michael's Admin Panel", template_mode="bootstrap3")

    # Add Meta Model views:
    admin.add_view(LocalizedPageView(models.LocalizedPage, db.session))
    admin.add_view(LocalizedProjectView(models.LocalizedProject, db.session))
    admin.add_view(ModelView(models.Page, db.session))
    admin.add_view(ModelView(models.Project, db.session))
    admin.add_view(ModelView(models.Language, db.session))

    return admin
