from flask import request
from flask_restx import Namespace, Resource

from mpm_api.api.models import Language
from mpm_api.api.models import LocalizedProject as Project

api = Namespace(
    "project",
    "This resource is for accessing the information for each project in my portfolio",
)

@api.route("/")
class AllProjects(Resource):
    def get(self):
        language = request.args.get("lang", "EN")

        try:
            projects = Project.get_all_by_language(
                Language.find_by_code(language)
            )
            projects_json = [project.json() for project in projects]
            return projects_json, 200

        except FileNotFoundError:
            return {"message": "language is missing"}, 404

        except Exception:
            return {"message": "something went wrong -- consult admin!"}, 500

@api.route("/<id>")
class LocalizedProject(Resource):
    def get(self, id):
        language = request.args.get("lang", "EN")
        try:
            project = Project.find_project_lang_combo(id, language)
            return project.json(True), 200

        except FileNotFoundError:
            return {"message": "project is missing"}, 404

        except KeyError:
            return {"message": "project-language pair is missing"}, 417

        except Exception:
            return {"message": "something went wrong -- consult admin!"}, 500
