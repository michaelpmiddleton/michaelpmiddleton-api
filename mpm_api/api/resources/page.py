from flask import request
from flask_restx import Namespace, Resource

from mpm_api.api.models import LocalizedPage as Page

api = Namespace(
    "page",
    "This resource is for accessing the information for each page on the site",
)


@api.route("/<id>")
class LocalizedPage(Resource):
    def get(self, id):
        language = request.args.get("lang", "EN")
        try:
            page = Page.find_page_lang_combo(id, language)
            return page.json(), 200

        except FileNotFoundError:
            return {"message": "page is missing"}, 404

        except KeyError:
            return {"message": "page-language pair is missing"}, 417

        except Exception:
            return {"message": "something went wrong -- consult admin!"}, 500
