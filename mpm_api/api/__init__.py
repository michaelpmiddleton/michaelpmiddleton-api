from flask import Blueprint
from flask_restx import Api

from mpm_api.api.resources import page, project

# Initialize the Blueprint & API objects
blueprint = Blueprint("Michael P. Middleton API", __name__)
api = Api(
    app=blueprint,
    version="1.0",
    description="This API powers all of the data that is fed to my personal website"
)

api.namespaces = []
api.add_namespace(page.api, path="/page")
api.add_namespace(project.api, path="/project")