from flask import Flask
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from mpm_api.api import models  # Get all models for
from mpm_api.main import app, db

# Establish the Manager and Migrate commands using the flask app created in main.py
migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command("db", MigrateCommand)

# Allow access to the API manager through the CLI
if __name__ == "__main__":
    manager.run()
