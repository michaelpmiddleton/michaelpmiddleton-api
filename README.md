# __MICHAELPMIDDLETON API__  
<!-- CI Badges go here. -->
This repository comprises the files that power the API which feeds data to/from my personal website.  

<br><br>

## _What's Up with the Branches?_
The different branches are used as follows:  

| Branch | Purpose |
| ------: | ------ |
| **master** | Currently live in PROD. Merges are squashed from *dev*. |
| **dev** | Upcoming, completed feautres currently live in PREPROD. Merges are squashed from *hyper_dev* | 
| **hyper_dev** | Feature development. Only ever live on my sandbox. | 

<br><br>

## _Want to Fork?_
Please feel free to use my code as a reference for your own work. With that said, at this time I am requesting that you do not _copy_ this code. Thank you for your understanding and happy hacking!  
<br><br>

## _Got Questions?_ 
I'm happy to answer them! Just [send me an email](mailto:mp.middleton@outlook.com)!
